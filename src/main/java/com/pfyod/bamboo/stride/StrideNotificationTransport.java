package com.pfyod.bamboo.stride;

import com.atlassian.bamboo.author.Author;
import com.atlassian.bamboo.builder.BuildState;
import com.atlassian.bamboo.builder.LifeCycleState;
import com.atlassian.bamboo.deployments.results.DeploymentResult;
import com.atlassian.bamboo.notification.Notification;
import com.atlassian.bamboo.notification.NotificationTransport;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.utils.HttpUtils;
import com.atlassian.bamboo.variable.CustomVariableContext;
import org.apache.http.HttpHost;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;

import org.apache.commons.lang.StringUtils;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.*;
import org.jsoup.safety.Whitelist;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

public class StrideNotificationTransport implements NotificationTransport
{
    private static final Logger log = Logger.getLogger(StrideNotificationTransport.class);

    // Can either be one of 'info', 'note', 'warning', 'success' or 'error'
    public static final String PANEL_TYPE_UNKNOWN_STATE = "note";
    public static final String PANEL_TYPE_FAILED = "error";
    public static final String PANEL_TYPE_SUCCESSFUL = "success";
    public static final String PANEL_TYPE_IN_PROGRESS = "info";

    private final String accessToken;
    private final String conversationURL;

    private CloseableHttpClient client;

    @Nullable
    private final ImmutablePlan plan;
    @Nullable
    private final ResultsSummary resultsSummary;
    @Nullable
    private final DeploymentResult deploymentResult;

    public StrideNotificationTransport(String accessToken,
                                       String conversationURL,
                                       @Nullable ImmutablePlan plan,
                                       @Nullable ResultsSummary resultsSummary,
                                       @Nullable DeploymentResult deploymentResult,
                                       CustomVariableContext customVariableContext)
    {
        this.accessToken = customVariableContext.substituteString(accessToken);
        this.conversationURL = customVariableContext.substituteString(conversationURL);
        this.plan = plan;
        this.resultsSummary = resultsSummary;
        this.deploymentResult = deploymentResult;

        URI uri;
        try
        {
            uri = new URI(this.conversationURL);
        }
        catch (URISyntaxException e)
        {
            log.error("Unable to set up proxy settings, invalid URI encountered: " + e);
            return;
        }

        HttpUtils.EndpointSpec proxyForScheme = HttpUtils.getProxyForScheme(uri.getScheme());
        if (proxyForScheme!=null)
        {
            HttpHost proxy = new HttpHost(proxyForScheme.host, proxyForScheme.port);
            DefaultProxyRoutePlanner routePlanner = new DefaultProxyRoutePlanner(proxy);
            this.client = HttpClients.custom().setRoutePlanner(routePlanner).build();
        }
        else
        {
            this.client = HttpClients.createDefault();
        }
    }

    /**
     * Convert Bamboo HTML to Stride style. We drop any custom elements and rewrite the <a> elements with stride style.
     *
     * @param message bamboo message in html
     * @return stride style message
     */
    public static String textMessage(String message, String panelType) throws JSONException {
        Document document = Jsoup.parseBodyFragment(Jsoup.clean(message, Whitelist.basic()));

        JSONObject root = new JSONObject();

        JSONObject body = new JSONObject();
        body.put("version", 1);
        body.put("type", "doc");

        root.put("body", body);

        JSONArray rootContent = new JSONArray();
        body.put("content", rootContent);

        JSONObject panel = new JSONObject();
        panel.put("type", "panel");
        JSONObject panelAttrs = new JSONObject();
        panelAttrs.put("panelType", panelType);
        panel.put("attrs", panelAttrs);

        rootContent.put(panel);

        JSONObject paragraph = new JSONObject();
        paragraph.put("type", "paragraph");
        JSONArray paragraphContent = new JSONArray();
        paragraph.put("content", paragraphContent);

        JSONArray panelContent = new JSONArray();
        panelContent.put(paragraph);
        panel.put("content", panelContent);

        for (Node node : document.body().childNodes()) {
            if (node instanceof TextNode) {
                String text = cleanupText(((TextNode) node).text());
                if (!StringUtils.isEmpty(text)) {
                    JSONObject textNode = new JSONObject();
                    textNode.put("type", "text");
                    textNode.put("text", text);
                    paragraphContent.put(textNode);
                }
            } else if (node instanceof Element) {
                String text = cleanupText(((Element) node).text());
                String href = node.attr("href");
                if (!StringUtils.isEmpty(text)) {
                    JSONObject textNode = new JSONObject();
                    textNode.put("type", "text");
                    textNode.put("text", text);
                    if (!StringUtils.isEmpty(href)) {
                        JSONArray marks = new JSONArray();
                        textNode.put("marks", marks);
                        JSONObject linkMark = new JSONObject();
                        linkMark.put("type", "link");
                        JSONObject attrs = new JSONObject();
                        attrs.put("href", href);
                        linkMark.put("attrs", attrs);
                        marks.put(linkMark);
                    }
                    paragraphContent.put(textNode);
                }
            }
        }
        return root.toString();
    }

    @NotNull
    private static String cleanupText(String text) {
        return text.replace("\n", "").replace("\u00a0", "").replace("&nbsp;", "");
    }

    public void sendNotification(@NotNull Notification notification)
    {

        if (client == null)
        {
            log.error("HTTP client is not set, see previous errors.");
            return;
        }

        String message = (notification instanceof Notification.HtmlImContentProvidingNotification)
                ? ((Notification.HtmlImContentProvidingNotification) notification).getHtmlImContent()
                : notification.getIMContent();

        if (!StringUtils.isEmpty(message))
        {
            try
            {
                HttpPost method = setupPostMethod();

                String panelType = PANEL_TYPE_UNKNOWN_STATE;

                if (resultsSummary != null) {

                    panelType = getPanelType(resultsSummary);

                    Set<Author> authors = resultsSummary.getUniqueAuthors();
                    if (!authors.isEmpty())
                    {
                        message += " Responsible Users: ";

                        ArrayList<String> usernames = new ArrayList<String>();

                        for (Author author: authors)
                        {
                            usernames.add(author.getFullName());
                        }

                        message += String.join(", ", usernames);
                    }
                } else if (deploymentResult != null) {
                    panelType = getPanelType(deploymentResult);
                }

                try {
                    method.setEntity(new StringEntity(textMessage(message, panelType), ContentType.APPLICATION_JSON));
                } catch (JSONException e) {
                    log.error("JSON construction error :" + e.getMessage(), e);
                }
                try {
                    log.debug(method.getURI().toString());
                    log.debug(method.getEntity().toString());
                    client.execute(method);
                } catch (IOException e) {
                    log.error("Error using Stride API: " + e.getMessage(), e);
                }
            }
            catch(URISyntaxException e)
            {
                log.error("Error parsing webhook url: " + e.getMessage(), e);
            }
        }
    }

    private String getPanelType(ResultsSummary result)
    {
        if (result.getBuildState() == BuildState.FAILED)
        {
            return PANEL_TYPE_FAILED;
        }
        else if (result.getBuildState() == BuildState.SUCCESS)
        {
            return PANEL_TYPE_SUCCESSFUL;
        }
        else if (LifeCycleState.isActive(result.getLifeCycleState()))
        {
            return PANEL_TYPE_IN_PROGRESS;
        }

        return PANEL_TYPE_UNKNOWN_STATE;
    }

    private String getPanelType(DeploymentResult deploymentResult)
    {
        if (deploymentResult.getDeploymentState() == BuildState.FAILED)
        {
            return PANEL_TYPE_FAILED;
        }
        else if (deploymentResult.getDeploymentState() == BuildState.SUCCESS)
        {
            return PANEL_TYPE_SUCCESSFUL;
        }
        else if (LifeCycleState.isActive(deploymentResult.getLifeCycleState()))
        {
            return PANEL_TYPE_IN_PROGRESS;
        }

        return PANEL_TYPE_UNKNOWN_STATE;
    }

    private HttpPost setupPostMethod() throws URISyntaxException
    {
        HttpPost post = new HttpPost(new URI(conversationURL));
        post.addHeader("Authorization", "Bearer " + accessToken);
        return post;
    }

}
