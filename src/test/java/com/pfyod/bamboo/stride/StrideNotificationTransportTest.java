package com.pfyod.bamboo.stride;

import com.atlassian.bamboo.notification.buildhung.BuildHungNotification;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.project.Project;
import com.atlassian.bamboo.variable.CustomVariableContext;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.lang.reflect.Field;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class StrideNotificationTransportTest
{
    private final String ACCESS_TOKEN = "accesstoken";
    private final String CONVERSATION_URL = "conversationurl";

    @Mock
    private Project project;
    @Mock
    private Plan plan;
    @Mock
    private CustomVariableContext customVariableContext;

    @Test
    public void testCorrectUrlsAreHit()
    {
        when(project.getKey()).thenReturn("BAM");
        when(plan.getProject()).thenReturn(project);
        when(plan.getBuildKey()).thenReturn("MAIN");
        when(plan.getName()).thenReturn("Main");
        when(customVariableContext.substituteString("accesstoken")).thenReturn("accesstoken");
        when(customVariableContext.substituteString("conversationurl")).thenReturn("conversationurl");

        final PlanResultKey planResultKey = PlanKeys.getPlanResultKey("BAM-MAIN", 3);

        BuildHungNotification notification = new BuildHungNotification()
        {
            public String getHtmlImContent()
            {
                return "IM Content";
            }

        };

        StrideNotificationTransport hnt = new StrideNotificationTransport(ACCESS_TOKEN, CONVERSATION_URL, plan, null, null, customVariableContext);

        //dirty reflections trick to inject mock HttpClient
        try
        {
            Field field = StrideNotificationTransport.class.getDeclaredField("client");
            field.setAccessible(true);
            field.set(hnt, new MockHttpClient());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            fail(e.getMessage());
        }

        hnt.sendNotification(notification);
    }

    public class MockHttpClient extends CloseableHttpClient {

        @Override
        protected CloseableHttpResponse doExecute(HttpHost target, HttpRequest request, HttpContext context) throws IOException, ClientProtocolException {
            assertTrue(request instanceof HttpPost);
            HttpPost postMethod = (HttpPost) request;
            assertEquals(CONVERSATION_URL, request.getRequestLine().getUri());
            assertEquals(ACCESS_TOKEN, request.getFirstHeader("Authorization").getValue().substring("Bearer ".length()));

            try {
                assert(postMethod.getEntity() instanceof StringEntity);
                StringEntity entity = (StringEntity) postMethod.getEntity();
                JSONObject payload = new JSONObject(IOUtils.toString(entity.getContent()));
                assertTrue(payload.has("body"));
            } catch (JSONException e) {
                e.printStackTrace();
                fail(e.getMessage());
            }

            return null;
        }

        public void close() throws IOException {

        }

        public HttpParams getParams() {
            return null;
        }

        public ClientConnectionManager getConnectionManager() {
            return null;
        }
    }
}
