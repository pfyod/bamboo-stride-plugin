package com.pfyod.bamboo.stride;

import static junit.framework.Assert.assertEquals;

import org.json.JSONException;
import org.junit.Test;

public class StrideMessageParsingTest {

    private final String INPUT = "<img src='http://bamboo.int/bamboo/images/iconsv4/icon-build-successful.png' height='16' width='16' align='absmiddle' />&nbsp;<a href='http://bamboo.int/bamboo/browse/TEST-TEST-8'>test &rsaquo; test &rsaquo; #8</a> passed. Manual run by <a href=\"http://bamboo.int/bamboo/browse/user/admin\">admin</a>";
    private final String OUTPUT_TEXT = "{\"body\":{\"type\":\"doc\",\"version\":1,\"content\":[{\"type\":\"panel\",\"content\":[{\"type\":\"paragraph\",\"content\":[{\"text\":\" \",\"type\":\"text\"},{\"text\":\"test \\u203a test \\u203a #8\",\"marks\":[{\"type\":\"link\",\"attrs\":{\"href\":\"http://bamboo.int/bamboo/browse/TEST-TEST-8\"}}],\"type\":\"text\"},{\"text\":\" passed. Manual run by \",\"type\":\"text\"},{\"text\":\"admin\",\"marks\":[{\"type\":\"link\",\"attrs\":{\"href\":\"http://bamboo.int/bamboo/browse/user/admin\"}}],\"type\":\"text\"}]}],\"attrs\":{\"panelType\":\"success\"}}]}}";

    private final String BRANCH_INPUT = "<img src='http://bamboo.int/bamboo/images/iconsv4/icon-build-failed.png' height='16' width='16' align='absmiddle' />&nbsp;<a href='http://bamboo.int/bamboo/browse/AA-SLAP0-5'>Atlassian Anarchy &rsaquo; Sounds like a plan &rsaquo; <img src='http://bamboo.int/bamboo/images/icons/branch.png' height='16' width='16' align='absmiddle' />&nbsp;test-branch &rsaquo; #5</a> failed. Manual run by <a href=\"http://bamboo.int/bamboo/browse/user/admin\">Admin</a>\n";
    private final String BRANCH_OUTPUT_TEXT = "{\"body\":{\"type\":\"doc\",\"version\":1,\"content\":[{\"type\":\"panel\",\"content\":[{\"type\":\"paragraph\",\"content\":[{\"text\":\" \",\"type\":\"text\"},{\"text\":\"Atlassian Anarchy \\u203a Sounds like a plan \\u203a test-branch \\u203a #5\",\"marks\":[{\"type\":\"link\",\"attrs\":{\"href\":\"http://bamboo.int/bamboo/browse/AA-SLAP0-5\"}}],\"type\":\"text\"},{\"text\":\" failed. Manual run by \",\"type\":\"text\"},{\"text\":\"Admin\",\"marks\":[{\"type\":\"link\",\"attrs\":{\"href\":\"http://bamboo.int/bamboo/browse/user/admin\"}}],\"type\":\"text\"}]}],\"attrs\":{\"panelType\":\"error\"}}]}}";

    private final String MAIL_INPUT = "<img src='http://bamboo.int/bamboo/images/iconsv4/icon-build-successful.png' height='16' width='16' align='absmiddle' />&nbsp;<a href='http://bamboo.int/bamboo/browse/DEMO-DP-8'>DEMO &rsaquo; DEMO PLAN &rsaquo; #8</a> passed. Manual run by <a href=\"http://bamboo.int/bamboo/browse/user/mattias\">Räksmörgås RÄKSMÖRGÅS  &lt;raksmargas@gmail.com&gt;</a>";
    private final String MAIL_OUTPUT_TEXT = "{\"body\":{\"type\":\"doc\",\"version\":1,\"content\":[{\"type\":\"panel\",\"content\":[{\"type\":\"paragraph\",\"content\":[{\"text\":\" \",\"type\":\"text\"},{\"text\":\"DEMO \\u203a DEMO PLAN \\u203a #8\",\"marks\":[{\"type\":\"link\",\"attrs\":{\"href\":\"http://bamboo.int/bamboo/browse/DEMO-DP-8\"}}],\"type\":\"text\"},{\"text\":\" passed. Manual run by \",\"type\":\"text\"},{\"text\":\"Räksmörgås RÄKSMÖRGÅS <raksmargas@gmail.com>\",\"marks\":[{\"type\":\"link\",\"attrs\":{\"href\":\"http://bamboo.int/bamboo/browse/user/mattias\"}}],\"type\":\"text\"}]}],\"attrs\":{\"panelType\":\"success\"}}]}}";

    private final String UNKNOWN_USERNAME_INPUT = "<img src='http://bamboo.int/bamboo/images/iconsv4/icon-build-successful.png' height='16' width='16' align='absmiddle' />&nbsp;<a href='http://bamboo.int/bamboo/browse/DEMO-DP-8'>DEMO &rsaquo; DEMO PLAN &rsaquo; #8</a> passed. Manual run by <a href=\"http://bamboo.int/bamboo/browse/user/unknown\">[unknown]</a>";
    private final String UNKNOWN_USERNAME_OUTPUT_TEXT = "{\"body\":{\"type\":\"doc\",\"version\":1,\"content\":[{\"type\":\"panel\",\"content\":[{\"type\":\"paragraph\",\"content\":[{\"text\":\" \",\"type\":\"text\"},{\"text\":\"DEMO \\u203a DEMO PLAN \\u203a #8\",\"marks\":[{\"type\":\"link\",\"attrs\":{\"href\":\"http://bamboo.int/bamboo/browse/DEMO-DP-8\"}}],\"type\":\"text\"},{\"text\":\" passed. Manual run by \",\"type\":\"text\"},{\"text\":\"[unknown]\",\"marks\":[{\"type\":\"link\",\"attrs\":{\"href\":\"http://bamboo.int/bamboo/browse/user/unknown\"}}],\"type\":\"text\"}]}],\"attrs\":{\"panelType\":\"success\"}}]}}";

    private final String DEPLOY_INPUT = "<img src='http://bamboo.int/bamboo/images/iconsv4/icon-build-queued.png'/>&nbsp;<a href=\"http://bamboo.int/bamboo/deploy/viewDeploymentProjectEnvironments.action?id=3309569\">demo</a>  <a href=\"http://bamboo.int/bamboo/deploy/viewDeploymentVersion.action?versionId=3571714\">demo-release-2</a> has started deploying to <a href=\"http://bamboo.int/bamboo/deploy/viewEnvironment.action?id=3375105\">Development</a>. <a href=\"http://bamboo.int/bamboo/deploy/viewDeploymentResult.action?deploymentResultId=3833861\">See details</a>.";
    private final String DEPLOY_OUTPUT = "{\"body\":{\"type\":\"doc\",\"version\":1,\"content\":[{\"type\":\"panel\",\"content\":[{\"type\":\"paragraph\",\"content\":[{\"text\":\" \",\"type\":\"text\"},{\"text\":\"demo\",\"marks\":[{\"type\":\"link\",\"attrs\":{\"href\":\"http://bamboo.int/bamboo/deploy/viewDeploymentProjectEnvironments.action?id=3309569\"}}],\"type\":\"text\"},{\"text\":\" \",\"type\":\"text\"},{\"text\":\"demo-release-2\",\"marks\":[{\"type\":\"link\",\"attrs\":{\"href\":\"http://bamboo.int/bamboo/deploy/viewDeploymentVersion.action?versionId=3571714\"}}],\"type\":\"text\"},{\"text\":\" has started deploying to \",\"type\":\"text\"},{\"text\":\"Development\",\"marks\":[{\"type\":\"link\",\"attrs\":{\"href\":\"http://bamboo.int/bamboo/deploy/viewEnvironment.action?id=3375105\"}}],\"type\":\"text\"},{\"text\":\". \",\"type\":\"text\"},{\"text\":\"See details\",\"marks\":[{\"type\":\"link\",\"attrs\":{\"href\":\"http://bamboo.int/bamboo/deploy/viewDeploymentResult.action?deploymentResultId=3833861\"}}],\"type\":\"text\"},{\"text\":\".\",\"type\":\"text\"}]}],\"attrs\":{\"panelType\":\"info\"}}]}}";

    private final String DEPLOY_SPECIAL_INPUT = "<img src='http://bamboo.int/bamboo/images/iconsv4/icon-build-queued.png'/>&nbsp;<a href=\"http://bamboo.int/bamboo/deploy/viewDeploymentProjectEnvironments.action?id=3309569\">demo</a>  <a href=\"http://bamboo.int/bamboo/deploy/viewDeploymentVersion.action?versionId=3571714\">demo-release-2</a> has started deploying to <a href=\"http://bamboo.int/bamboo/deploy/viewEnvironment.action?id=3375105\">DEV - 2) Trigger Solr Backup</a>. <a href=\"http://bamboo.int/bamboo/deploy/viewDeploymentResult.action?deploymentResultId=3833861\">See details</a>.";
    private final String DEPLOY_SPECIAL_OUTPUT = "{\"body\":{\"type\":\"doc\",\"version\":1,\"content\":[{\"type\":\"panel\",\"content\":[{\"type\":\"paragraph\",\"content\":[{\"text\":\" \",\"type\":\"text\"},{\"text\":\"demo\",\"marks\":[{\"type\":\"link\",\"attrs\":{\"href\":\"http://bamboo.int/bamboo/deploy/viewDeploymentProjectEnvironments.action?id=3309569\"}}],\"type\":\"text\"},{\"text\":\" \",\"type\":\"text\"},{\"text\":\"demo-release-2\",\"marks\":[{\"type\":\"link\",\"attrs\":{\"href\":\"http://bamboo.int/bamboo/deploy/viewDeploymentVersion.action?versionId=3571714\"}}],\"type\":\"text\"},{\"text\":\" has started deploying to \",\"type\":\"text\"},{\"text\":\"DEV - 2) Trigger Solr Backup\",\"marks\":[{\"type\":\"link\",\"attrs\":{\"href\":\"http://bamboo.int/bamboo/deploy/viewEnvironment.action?id=3375105\"}}],\"type\":\"text\"},{\"text\":\". \",\"type\":\"text\"},{\"text\":\"See details\",\"marks\":[{\"type\":\"link\",\"attrs\":{\"href\":\"http://bamboo.int/bamboo/deploy/viewDeploymentResult.action?deploymentResultId=3833861\"}}],\"type\":\"text\"},{\"text\":\".\",\"type\":\"text\"}]}],\"attrs\":{\"panelType\":\"success\"}}]}}";

    @Test
    public void testTextMessage() throws JSONException {
        String output = StrideNotificationTransport.textMessage(INPUT, "success");
        String branch_output = StrideNotificationTransport.textMessage(BRANCH_INPUT, "error");
        assertEquals(OUTPUT_TEXT, output);
        assertEquals(BRANCH_OUTPUT_TEXT, branch_output);
    }

    /**
     * Test that we can properly handle messages for users that has an email adress as part of their username.
     * This typically appears if users in Bamboo is not a 100% match with users in the repo's use, for example:
     * - bitbucket
     * - github   
     * - etc.
     * The same issue appears if someone adds <foo@bar.com> to their users real name.
     */
    @Test
    public void testExternalUser() throws JSONException {
        final String mailOutput = StrideNotificationTransport.textMessage(MAIL_INPUT, "success");
        assertEquals(MAIL_OUTPUT_TEXT, mailOutput);
    }

    /**
     * Test that we can properly handle messages where the user is [unknown].
     * This seems to happen when a plan has been triggered by repository changes but Bamboo fails to
     * correctly parse the revision history and cannot assign a user.
     */
    @Test
    public void testUnknownUser() throws JSONException {
        final String unknownUserOutput = StrideNotificationTransport.textMessage(UNKNOWN_USERNAME_INPUT, "success");
        assertEquals(UNKNOWN_USERNAME_OUTPUT_TEXT, unknownUserOutput);
    }

    /**
     * Test that we can handle deployment messages even if the the Bamboo build plan is a subset of the release name.
     */
    @Test
    public void testDeploymentMessage() throws JSONException {
        assertEquals(DEPLOY_OUTPUT, StrideNotificationTransport.textMessage(DEPLOY_INPUT, "info"));
    }

    /**
     * Test that we can handle deployments to environments with special characters in their name.
     */
    @Test
    public void testDeploymentMessageSpecialChars() throws JSONException {
        assertEquals(DEPLOY_SPECIAL_OUTPUT, StrideNotificationTransport.textMessage(DEPLOY_SPECIAL_INPUT, "success"));
    }
}
