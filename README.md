Notifications from Bamboo to Stride
==============================

This plugin sends [Bamboo](https://www.atlassian.com/software/bamboo) notifications to [Stride](https://www.stride.com/).

You can get it [here](https://marketplace.atlassian.com/plugins/com.pfyod.bamboo.plugins.bamboo-stride)

It allows any Bamboo notification to be posted to a specific stride room.

This plugin allows you to take advantage of Bamboo's:

-	flexible notification system (ie tell me when this build fails more than 5 times!)
-	commenting (comments show up in the chatroom so everyone can see someone's working on the build)

Based on [Slack Notifications for Bamboo](https://bitbucket.org/marache/bamboo-slack-plugin) by CSTB

Notifications Supported
-----------------------

-	Build successful
-	Build failed
-	Build commented
-	Job hung
-	Job queue timeout

Setup
-----

1.	Go to the *Notifications* tab of the *Configure Plan* screen.
2.	Choose a *Recipient Type* of *Stride*
3.	Configure your *Stride access token* and *Stride conversation URL*
4.	You're done! Go and get building.

Compiling from source
---------------------

You first need to [Set up the Atlassian Plugin SDK](https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project). Or you could just do a `brew tap atlassian/tap; brew install atlassian/tap/atlassian-plugin-sdk` on a mac is you use HomeBrew... At the project top level (where the pom.xml is) :

1.	Compile : `atlas-mvn compile`
2.	Run : `atlas-run`
3.	Debug : `atlas-debug`

Feedback? Questions?
--------------------

paul.fyodorov@gmail.com.